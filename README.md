# Mixly for Web

### 1.部署Mixly for Web工程(先安装驱动)
打开config.ini
```
[Web]
Title=web服务器已启动
Host=
Port=8081
view=1

[Arduino]
COM=3
AppID=B50071DB9F00635AA836E9DD6A7FA44F
```
Web中配置项对应关系:

| Tilte        | Host           |Port| view  |
| ----------- |:-------------:| -----:| -----:|
| 启动后的标题  | 主机名，默认为空(**全部未分配**) | 端口号 |是否在命令行上显示提示（ **1** 为是）|

Arduino中配置项对应关系:

| COM       | AppID          |
| ----------- |:-------------:|
| Arduino的串口端口号  | 通信密钥,无需修改 | 

----
### 2.打开 ArduinoPost.exe,并连接Arduino
![2](https://git.oschina.net/uploads/images/2017/0825/183057_41cec28b_906045.png "2.png")
![3](https://git.oschina.net/uploads/images/2017/0825/183518_0bd8eeb3_906045.jpeg "3.jpg")
----
### 3.访问
```
http://你的IP或域名:你的端口
```
点击 **Click here!** 

![4](https://git.oschina.net/uploads/images/2017/0825/183731_fb421cb9_906045.png "4.png")

编写程序,点击 **Write** 烧写至你的Arduino

![5](https://git.oschina.net/uploads/images/2017/0825/184124_65bd4426_906045.png "5.png")

如果config.ini中 view=1,命令提示符中会有如下提示

![6](https://git.oschina.net/uploads/images/2017/0825/184357_39b5eb75_906045.png "6.png")
